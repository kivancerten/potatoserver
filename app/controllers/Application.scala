package controllers

import models.HttpTransaction.HttpTransaction
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

object Application extends Controller with MongoController {

  def collection: JSONCollection = db.collection[JSONCollection]("httpTransaction")

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def saveHttpTransaction = Action.async(parse.json) { request =>

    request.body.validate[HttpTransaction].map { httpTransaction =>
      collection.insert(httpTransaction).map { lastError =>
        Created
      }
    }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

}