package models

import play.api.libs.json.Json

/**
 * @author  Kivanc Erten <kerten@ebay.com>
 */
object HttpTransaction {

  case class HttpTransaction(request:String, response:String)

  implicit val httpTransactionWrites = Json.writes[HttpTransaction]
  implicit val httpTransactionReads = Json.reads[HttpTransaction]

}
